#include "input.h"
#include "SDL/SDL.h"

me_input setupInput(int screen_width, int screen_height){
    struct me_input input;
    int i;

    //init all the keys to not pressed
    for(i=0; i<105; i++)
    {
        input.keys[i] = false;
    }
    input.quit = false;
	input.mouseRelX = 0;
	input.mouseRelY = 0;
	input.mouseX = 0;
	input.mouseY = 0;
    input.screenWidth = screen_width;
    input.screenHeight = screen_height;
    return input;
}

bool me_input::isDown(int keyCodeEnum) {
    if(keys[keyCodeEnum])
        return true;
    else
        return false;
}

bool me_input::isUp(int keyCodeEnum) {
    if(keys[keyCodeEnum])
        return false;
    else
        return true;
}

void processInput(me_input &input){
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        switch(event.type) {
            case SDL_QUIT:
                input.quit = true;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym){
                    case SDLK_UNKNOWN:
                        input.keys[0] = true;
                        break;
                    case SDLK_BACKSPACE:
                        input.keys[1] = true;
                        break;
                    case SDLK_TAB:
                        input.keys[2] = true;
                        break;
                    case SDLK_RETURN:
                        input.keys[3] = true;
                        break;
                    case SDLK_ESCAPE:
                        input.keys[4] = true;
                        break;
                    case SDLK_SPACE:
                        input.keys[5] = true;
                        break;
                    case SDLK_EXCLAIM:
                        input.keys[6] = true;
                        break;
                    case SDLK_QUOTEDBL:
                        input.keys[7] = true;
                        break;
                    case SDLK_HASH:
                        input.keys[8] = true;
                        break;
                    case SDLK_DOLLAR:
                        input.keys[9] = true;
                        break;
                    //PERCENT DOESNT WORK!
                    case SDLK_AMPERSAND:
                        input.keys[11] = true;
                        break;
                    case SDLK_QUOTE:
                        input.keys[12] = true;
                        break;
                    case SDLK_LEFTPAREN:
                        input.keys[13] = true;
                        break;
                    case SDLK_RIGHTPAREN:
                        input.keys[14] = true;
                        break;
                    case SDLK_ASTERISK:
                        input.keys[15] = true;
                        break;
                    case SDLK_PLUS:
                        input.keys[16] = true;
                        break;
                    case SDLK_COMMA:
                        input.keys[17] = true;
                        break;
                    case SDLK_MINUS:
                        input.keys[18] = true;
                        break;
                    case SDLK_PERIOD:
                        input.keys[19] = true;
                        break;
                    case SDLK_SLASH:
                        input.keys[20] = true;
                        break;
                    case SDLK_0:
                        input.keys[21] = true;
                        break;
                    case SDLK_1:
                        input.keys[22] = true;
                        break;
                    case SDLK_2:
                        input.keys[23] = true;
                        break;
                    case SDLK_3:
                        input.keys[24] = true;
                        break;
                    case SDLK_4:
                        input.keys[25] = true;
                        break;
                    case SDLK_5:
                        input.keys[26] = true;
                        break;
                    case SDLK_6:
                        input.keys[27] = true;
                        break;
                    case SDLK_7:
                        input.keys[28] = true;
                        break;
                    case SDLK_8:
                        input.keys[29] = true;
                        break;
                    case SDLK_9:
                        input.keys[30] = true;
                        break;
                    case SDLK_COLON:
                        input.keys[31] = true;
                        break;
                    case SDLK_SEMICOLON:
                        input.keys[32] = true;
                        break;
                    case SDLK_LESS:
                        input.keys[33] = true;
                        break;
                    case SDLK_EQUALS:
                        input.keys[34] = true;
                        break;
                    case SDLK_GREATER:
                        input.keys[35] = true;
                        break;
                    case SDLK_QUESTION:
                        input.keys[36] = true;
                        break;
                    case SDLK_AT:
                        input.keys[37] = true;
                        break;
                    case SDLK_LEFTBRACKET:
                        input.keys[38] = true;
                        break;
                    case SDLK_BACKSLASH:
                        input.keys[39] = true;
                        break;
                    case SDLK_RIGHTBRACKET:
                        input.keys[40] = true;
                        break;
                    case SDLK_CARET:
                        input.keys[41] = true;
                        break;
                    case SDLK_UNDERSCORE:
                        input.keys[42] = true;
                        break;
                    case SDLK_BACKQUOTE:
                        input.keys[43] = true;
                        break;
                    case SDLK_a:
                        input.keys[44] = true;
                        break;
                    case SDLK_b:
                        input.keys[45] = true;
                        break;
                    case SDLK_c:
                        input.keys[46] = true;
                        break;
                    case SDLK_d:
                        input.keys[47] = true;
                        break;
                    case SDLK_e:
                        input.keys[48] = true;
                        break;
                    case SDLK_f:
                        input.keys[49] = true;
                        break;
                    case SDLK_g:
                        input.keys[50] = true;
                        break;
                    case SDLK_h:
                        input.keys[51] = true;
                        break;
                    case SDLK_i:
                        input.keys[52] = true;
                        break;
                    case SDLK_j:
                        input.keys[53] = true;
                        break;
                    case SDLK_k:
                        input.keys[54] = true;
                        break;
                    case SDLK_l:
                        input.keys[55] = true;
                        break;
                    case SDLK_m:
                        input.keys[56] = true;
                        break;
                    case SDLK_n:
                        input.keys[57] = true;
                        break;
                    case SDLK_o:
                        input.keys[58] = true;
                        break;
                    case SDLK_p:
                        input.keys[59] = true;
                        break;
                    case SDLK_q:
                        input.keys[60] = true;
                        break;
                    case SDLK_r:
                        input.keys[61] = true;
                        break;
                    case SDLK_s:
                        input.keys[62] = true;
                        break;
                    case SDLK_t:
                        input.keys[63] = true;
                        break;
                    case SDLK_u:
                        input.keys[64] = true;
                        break;
                    case SDLK_v:
                        input.keys[65] = true;
                        break;
                    case SDLK_w:
                        input.keys[66] = true;
                        break;
                    case SDLK_x:
                        input.keys[67] = true;
                        break;
                    case SDLK_y:
                        input.keys[68] = true;
                        break;
                    case SDLK_z:
                        input.keys[69] = true;
                        break;
                    case SDLK_DELETE:
                        input.keys[70] = true;
                        break;
                    case SDLK_CAPSLOCK:
                        input.keys[71] = true;
                        break;
                    case SDLK_F1:
                        input.keys[72] = true;
                        break;
                    case SDLK_F2:
                        input.keys[73] = true;
                        break;
                    case SDLK_F3:
                        input.keys[74] = true;
                        break;
                    case SDLK_F4:
                        input.keys[75] = true;
                        break;
                    case SDLK_F5:
                        input.keys[76] = true;
                        break;
                    case SDLK_F6:
                        input.keys[77] = true;
                        break;
                    case SDLK_F7:
                        input.keys[78] = true;
                        break;
                    case SDLK_F8:
                        input.keys[79] = true;
                        break;
                    case SDLK_F9:
                        input.keys[80] = true;
                        break;
                    case SDLK_F10:
                        input.keys[81] = true;
                        break;
                    case SDLK_F11:
                        input.keys[82] = true;
                        break;
                    case SDLK_F12:
                        input.keys[83] = true;
                        break;
                    //PRINTSCREEN DOESNT WORK!
                    //SCROLLLOCK DOESNT WORK!
                    case SDLK_PAUSE:
                        input.keys[86] = true;
                        break;
                    case SDLK_INSERT:
                        input.keys[87] = true;
                        break;
                    case SDLK_HOME:
                        input.keys[88] = true;
                        break;
                    case SDLK_PAGEUP:
                        input.keys[89] = true;
                        break;
                    case SDLK_END:
                        input.keys[90] = true;
                        break;
                    case SDLK_PAGEDOWN:
                        input.keys[91] = true;
                        break;
                    case SDLK_RIGHT:
                        input.keys[92] = true;
                        break;
                    case SDLK_LEFT:
                        input.keys[93] = true;
                        break;
                    case SDLK_DOWN:
                        input.keys[94] = true;
                        break;
                    case SDLK_UP:
                        input.keys[95] = true;
                        break;
                    //NUMLOCK DOESNT WORK!
                    case SDLK_LCTRL:
                        input.keys[97] = true;
                        break;
                    case SDLK_LSHIFT:
                        input.keys[98] = true;
                        break;
                    case SDLK_LALT:
                        input.keys[99] = true;
                        break;
                    case SDLK_LMETA:
                        input.keys[100] = true;
                        break;
                    case SDLK_LSUPER:
                        input.keys[100] = true;
                        break;
                    case SDLK_RCTRL:
                        input.keys[101] = true;
                        break;
                    case SDLK_RSHIFT:
                        input.keys[102] = true;
                        break;
                    case SDLK_RALT:
                        input.keys[103] = true;
                        break;
                    case SDLK_RMETA:
                        input.keys[104] = true;
                        break;
                    case SDLK_RSUPER:
                        input.keys[104] = true;
                        break;
                    default:
                        input.keys[0] = true;
                        break;
                }
                break;
            case SDL_KEYUP:
                switch(event.key.keysym.sym){
                    case SDLK_UNKNOWN:
                        input.keys[0] = false;
                        break;
                    case SDLK_BACKSPACE:
                        input.keys[1] = false;
                        break;
                    case SDLK_TAB:
                        input.keys[2] = false;
                        break;
                    case SDLK_RETURN:
                        input.keys[3] = false;
                        break;
                    case SDLK_ESCAPE:
                        input.keys[4] = false;
                        break;
                    case SDLK_SPACE:
                        input.keys[5] = false;
                        break;
                    case SDLK_EXCLAIM:
                        input.keys[6] = false;
                        break;
                    case SDLK_QUOTEDBL:
                        input.keys[7] = false;
                        break;
                    case SDLK_HASH:
                        input.keys[8] = false;
                        break;
                    case SDLK_DOLLAR:
                        input.keys[9] = false;
                        break;
                    //PERCENT DOESNT WORK!
                    case SDLK_AMPERSAND:
                        input.keys[11] = false;
                        break;
                    case SDLK_QUOTE:
                        input.keys[12] = false;
                        break;
                    case SDLK_LEFTPAREN:
                        input.keys[13] = false;
                        break;
                    case SDLK_RIGHTPAREN:
                        input.keys[14] = false;
                        break;
                    case SDLK_ASTERISK:
                        input.keys[15] = false;
                        break;
                    case SDLK_PLUS:
                        input.keys[16] = false;
                        break;
                    case SDLK_COMMA:
                        input.keys[17] = false;
                        break;
                    case SDLK_MINUS:
                        input.keys[18] = false;
                        break;
                    case SDLK_PERIOD:
                        input.keys[19] = false;
                        break;
                    case SDLK_SLASH:
                        input.keys[20] = false;
                        break;
                    case SDLK_0:
                        input.keys[21] = false;
                        break;
                    case SDLK_1:
                        input.keys[22] = false;
                        break;
                    case SDLK_2:
                        input.keys[23] = false;
                        break;
                    case SDLK_3:
                        input.keys[24] = false;
                        break;
                    case SDLK_4:
                        input.keys[25] = false;
                        break;
                    case SDLK_5:
                        input.keys[26] = false;
                        break;
                    case SDLK_6:
                        input.keys[27] = false;
                        break;
                    case SDLK_7:
                        input.keys[28] = false;
                        break;
                    case SDLK_8:
                        input.keys[29] = false;
                        break;
                    case SDLK_9:
                        input.keys[30] = false;
                        break;
                    case SDLK_COLON:
                        input.keys[31] = false;
                        break;
                    case SDLK_SEMICOLON:
                        input.keys[32] = false;
                        break;
                    case SDLK_LESS:
                        input.keys[33] = false;
                        break;
                    case SDLK_EQUALS:
                        input.keys[34] = false;
                        break;
                    case SDLK_GREATER:
                        input.keys[35] = false;
                        break;
                    case SDLK_QUESTION:
                        input.keys[36] = false;
                        break;
                    case SDLK_AT:
                        input.keys[37] = false;
                        break;
                    case SDLK_LEFTBRACKET:
                        input.keys[38] = false;
                        break;
                    case SDLK_BACKSLASH:
                        input.keys[39] = false;
                        break;
                    case SDLK_RIGHTBRACKET:
                        input.keys[40] = false;
                        break;
                    case SDLK_CARET:
                        input.keys[41] = false;
                        break;
                    case SDLK_UNDERSCORE:
                        input.keys[42] = false;
                        break;
                    case SDLK_BACKQUOTE:
                        input.keys[43] = false;
                        break;
                    case SDLK_a:
                        input.keys[44] = false;
                        break;
                    case SDLK_b:
                        input.keys[45] = false;
                        break;
                    case SDLK_c:
                        input.keys[46] = false;
                        break;
                    case SDLK_d:
                        input.keys[47] = false;
                        break;
                    case SDLK_e:
                        input.keys[48] = false;
                        break;
                    case SDLK_f:
                        input.keys[49] = false;
                        break;
                    case SDLK_g:
                        input.keys[50] = false;
                        break;
                    case SDLK_h:
                        input.keys[51] = false;
                        break;
                    case SDLK_i:
                        input.keys[52] = false;
                        break;
                    case SDLK_j:
                        input.keys[53] = false;
                        break;
                    case SDLK_k:
                        input.keys[54] = false;
                        break;
                    case SDLK_l:
                        input.keys[55] = false;
                        break;
                    case SDLK_m:
                        input.keys[56] = false;
                        break;
                    case SDLK_n:
                        input.keys[57] = false;
                        break;
                    case SDLK_o:
                        input.keys[58] = false;
                        break;
                    case SDLK_p:
                        input.keys[59] = false;
                        break;
                    case SDLK_q:
                        input.keys[60] = false;
                        break;
                    case SDLK_r:
                        input.keys[61] = false;
                        break;
                    case SDLK_s:
                        input.keys[62] = false;
                        break;
                    case SDLK_t:
                        input.keys[63] = false;
                        break;
                    case SDLK_u:
                        input.keys[64] = false;
                        break;
                    case SDLK_v:
                        input.keys[65] = false;
                        break;
                    case SDLK_w:
                        input.keys[66] = false;
                        break;
                    case SDLK_x:
                        input.keys[67] = false;
                        break;
                    case SDLK_y:
                        input.keys[68] = false;
                        break;
                    case SDLK_z:
                        input.keys[69] = false;
                        break;
                    case SDLK_DELETE:
                        input.keys[70] = false;
                        break;
                    case SDLK_CAPSLOCK:
                        input.keys[71] = false;
                        break;
                    case SDLK_F1:
                        input.keys[72] = false;
                        break;
                    case SDLK_F2:
                        input.keys[73] = false;
                        break;
                    case SDLK_F3:
                        input.keys[74] = false;
                        break;
                    case SDLK_F4:
                        input.keys[75] = false;
                        break;
                    case SDLK_F5:
                        input.keys[76] = false;
                        break;
                    case SDLK_F6:
                        input.keys[77] = false;
                        break;
                    case SDLK_F7:
                        input.keys[78] = false;
                        break;
                    case SDLK_F8:
                        input.keys[79] = false;
                        break;
                    case SDLK_F9:
                        input.keys[80] = false;
                        break;
                    case SDLK_F10:
                        input.keys[81] = false;
                        break;
                    case SDLK_F11:
                        input.keys[82] = false;
                        break;
                    case SDLK_F12:
                        input.keys[83] = false;
                        break;
                    //PRINTSCREEN DOESNT WORK!
                    //SCROLLLOCK DOESNT WORK!
                    case SDLK_PAUSE:
                        input.keys[86] = false;
                        break;
                    case SDLK_INSERT:
                        input.keys[87] = false;
                        break;
                    case SDLK_HOME:
                        input.keys[88] = false;
                        break;
                    case SDLK_PAGEUP:
                        input.keys[89] = false;
                        break;
                    case SDLK_END:
                        input.keys[90] = false;
                        break;
                    case SDLK_PAGEDOWN:
                        input.keys[91] = false;
                        break;
                    case SDLK_RIGHT:
                        input.keys[92] = false;
                        break;
                    case SDLK_LEFT:
                        input.keys[93] = false;
                        break;
                    case SDLK_DOWN:
                        input.keys[94] = false;
                        break;
                    case SDLK_UP:
                        input.keys[95] = false;
                        break;
                    //NUMLOCK DOESNT WORK!
                    case SDLK_LCTRL:
                        input.keys[97] = false;
                        break;
                    case SDLK_LSHIFT:
                        input.keys[98] = false;
                        break;
                    case SDLK_LALT:
                        input.keys[99] = false;
                        break;
                    case SDLK_LMETA:
                        input.keys[100] = false;
                        break;
                    case SDLK_LSUPER:
                        input.keys[100] = false;
                        break;
                    case SDLK_RCTRL:
                        input.keys[101] = false;
                        break;
                    case SDLK_RSHIFT:
                        input.keys[102] = false;
                        break;
                    case SDLK_RALT:
                        input.keys[103] = false;
                        break;
                    case SDLK_RMETA:
                        input.keys[104] = false;
                        break;
                    case SDLK_RSUPER:
                        input.keys[104] = false;
                        break;
                    default:
                        input.keys[0] = false;
                        break;
                }
            break;
			case SDL_MOUSEMOTION:
                input.mouseX = event.motion.x;
                input.mouseY = event.motion.y;
				input.mouseRelX = event.motion.xrel;
				input.mouseRelY = event.motion.yrel;
			break;
            case SDL_MOUSEBUTTONDOWN:
                switch (event.button.button) {
                    case SDL_BUTTON_LEFT:
                        input.keys[105] = true;
                        break;
                    case SDL_BUTTON_RIGHT:
                        input.keys[106] = true;
                        break;
                    case SDL_BUTTON_MIDDLE:
                        input.keys[107] = true;
                        break;
                    default:
                        break;
                }
            break;
            case SDL_MOUSEBUTTONUP:
                switch (event.button.button) {
                    case SDL_BUTTON_LEFT:
                        input.keys[105] = false;
                        break;
                    case SDL_BUTTON_RIGHT:
                        input.keys[106] = false;
                        break;
                    case SDL_BUTTON_MIDDLE:
                        input.keys[107] = false;
                        break;
                    default:
                        break;
                }
            break;
        }
    }
   
}