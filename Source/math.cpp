#include "math.h"

double lerp(double start, double end, double percent)
{
     return (start + (percent*(end - start)));
}

double clamp(double number, double bottom, double top)
{
    if(number > top)
        return top;
    else if(number < bottom)
        return bottom;
    else
        return number;
}