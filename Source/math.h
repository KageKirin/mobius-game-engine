#ifndef ME_MATH
#define ME_MATH

#define PI 3.14159265   
#define DEG2RAD 0.01745329
#define RAD2DEG 57.29577951

double lerp(double start, double end, double percent);
double clamp(double number, double bottom, double top);

#endif
