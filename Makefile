ifeq ($(OS),Windows_NT)
      #Windows stuff
    Libs = -lmingw32 -lSDLmain -lSDL -lOpenGl32 -lglu32 -mwindows -static-libstdc++ -static-libgcc
    Output = bin/Windows/mobius.exe
else
    #Linux stuff
    Libs = -lSDLmain -lSDL -lGL -lGLU
    Output = bin/Linux/mobius
endif

    mobius: Source/main.cpp
		g++ Source/main.cpp Source/input.cpp Source/math.cpp -o $(Output) $(Libs)

